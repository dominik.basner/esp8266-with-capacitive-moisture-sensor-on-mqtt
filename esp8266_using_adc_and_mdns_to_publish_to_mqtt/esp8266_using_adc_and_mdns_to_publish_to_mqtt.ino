#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>
#include "mqttconf.h"
#include "wificonf.h"
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"


int i;
int moisture = 0;  
int minimum_moisture = 816;
int maximum_moisture = 340; 
int nominator = maximum_moisture - minimum_moisture;
double moisture_percent = 0.0;

WiFiClient client;
Adafruit_MQTT_Client *mqtt = NULL;
Adafruit_MQTT_Publish *max6675_topic = NULL;


const int valve = 16;
const int led = 2;


void setup(void) {
  pinMode(led, OUTPUT);
  pinMode(valve, OUTPUT);

  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("test");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  Serial.println("mDNS responder started");

  Serial.println("Sending mDNS query");
  int n = MDNS.queryService("mqtt", "tcp"); // Send out query for esp tcp services
  Serial.println("mDNS query done");
  String mqtt_serverAddress = mqtt_server;
    if (n == 0) {
    Serial.println("no services found");
  } else {
    Serial.print(n);
    Serial.println(" service(s) found");

    for (int i = 0; i < n; ++i) {
      // Print details for each service found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(MDNS.hostname(i));
      Serial.print(" (");
      IPAddress IP = MDNS.IP(i);
      mqtt_serverAddress = IP.toString();
      Serial.print(MDNS.IP(i));
      Serial.print(":");
      Serial.print(MDNS.port(i));
      Serial.println(")");
    }
  }
  Serial.println(mqtt_serverAddress.c_str());

  MQTT_connect(mqtt_serverAddress.c_str());
  // wait for MAX chip!
  delay(500);

  Serial.println("loop() next");
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect(const char* mqtt_serverAddress) {
  mqtt = new Adafruit_MQTT_Client(&client, mqtt_serverAddress, mqtt_serverport, mqtt_username, mqtt_password);
  max6675_topic = new Adafruit_MQTT_Publish(mqtt, "/sensor1/moisture");
  int8_t ret;

  while (mqtt == NULL) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Trying to start MQTT...");

  // Stop if already connected.
  if (mqtt->connected()) {
    return;
  }


  Serial.println("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt->connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt->connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt->disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      while (1);
    }
  }
  Serial.println("MQTT Connected!");

}

void loop() {

  Serial.print("Humidity = ");

  moisture = analogRead(0); 
  moisture_percent = double (moisture - minimum_moisture) * 100 / nominator;
  String message2 = "Moisture: ";
  message2 += moisture_percent;
  message2 += "%";
  
  Serial.print(moisture_percent);

  delay(2500);
  max6675_topic->publish(moisture_percent);
  delay(2500);
  MDNS.update();

}
